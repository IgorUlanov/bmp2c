#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QTextStream>
#include <QDataStream>
#include <QTextCodec>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->setVisible(false);
    ui->rB_none->setChecked(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_all_clicked()
{
    // ковертация всех файлов из папки
    QStringList files= QFileDialog::getOpenFileNames(
                this,"open files",QDir::currentPath(),"Images (*.bmp)");
    QStringList::Iterator it = files.begin();
    ui->plainTextEdit->clear();
    while(it != files.end()) {
        ui->plainTextEdit->insertPlainText(*it+" -> "+saveFile(*it)+"\n");
        ++it;
    }
}

QString MainWindow::saveFile(QString name){
    quint32 bias,biWidth,biHeight;
    quint16 now;
    ui->progressBar->setValue(0);
    ui->progressBar->setVisible(true);
    QFile f_source(name);
    QFileInfo fi(name);
    QString name_cut=fi.fileName();
    name_cut=name_cut.left(name_cut.lastIndexOf(".",name_cut.size()-1));
    QString name_path=fi.absolutePath();
    name_path=name_path.left(name_path.lastIndexOf("/",name_path.size()-1)+1);
    QFile f_str("setting.txt");
    QFile f_dest(name_path+name_cut+".c");
    f_dest.open(QIODevice::WriteOnly | QIODevice::Text);
    f_str.open(QIODevice::ReadOnly | QIODevice::Text);
    f_source.open(QIODevice::ReadOnly);
    QTextStream destf(&f_dest);
    QTextStream str(&f_str);
    QDataStream source(&f_source);
    destf.setCodec(QTextCodec::codecForName("UTF-8"));
    str.setCodec(QTextCodec::codecForName("UTF-8"));
    source.setByteOrder(QDataStream::LittleEndian);
    source.skipRawData(10);
    source>>bias;
    source.skipRawData(4);
    source>>biWidth; // 240
    source>>biHeight; // 320
    source.skipRawData(bias-26);
    float step=50.0/biHeight;
    buf.clear();
    buf.resize(biHeight*biWidth);
    // картинка
    for (quint16 i=0;i<biHeight;i++){
        for (quint16 n=0;n<biWidth;n++){
            source>>now;
            buf.replace(n*biHeight+biHeight-i-1,now);
        }
        source.skipRawData((biWidth*sizeof(qint16))%4);
        ui->progressBar->setValue(biHeight*step);
    }
    // в выходной файл
    QString st="uint16_t";
    if (ui->checkBox->isChecked()) st="uint8_t";
    // голова
    while (!str.atEnd()){
        QString s=str.readLine();
        if (s=="") break;
        s.replace(QString("%name%"),name_cut);
        s.replace(QString("%type%"),st);
        destf << s << "\n";
    }
    QList<uint16_t> map;
    map.clear();
    dest.clear();
    if (ui->checkBox->isChecked()){
        // заархивировать
        fillDictionary();
        if (!ui->rB_none->isChecked()){
            // для шрифта создается таблица символов
            QString f_name="font_big.txt";
            if (ui->rB_lf->isChecked()) f_name="font_litlle.txt";
            else if (ui->rB_mf->isChecked()) f_name="font_middle.txt";
            QFile f_font(QDir::currentPath()+QDir::separator()+f_name);
            f_font.open(QIODevice::ReadOnly | QIODevice::Text);
            QTextStream fontf(&f_font);
            fontf.setCodec(QTextCodec::codecForName("UTF-8"));
            QString s;
            fontf>>s;
            QStringList sl=s.split(";");
            int h_font=sl[0].toUInt();
            int st=0;
            int fin=sl[1].toUInt();
            while(!fontf.atEnd()){
                fillArc(st*h_font,fin*h_font);
                map.append(dest.size());
                fontf>>s;
                if (s.isNull()) break;
                sl=s.split(";");
                st=sl[0].toUInt();
                fin=sl[1].toUInt();
            }
        } else fillArc(0,buf.size());
        // запись словаря
        for (int i=0;i<DICT;i++){
            if (i%8==0) destf<<"\n\t";
            destf << QString("0x%1,0x%2,")
                    .arg((uint8_t)dictionary[i],2,16,QLatin1Char('0'))
                    .arg((uint8_t)(dictionary[i]>>8),2,16,QLatin1Char('0'));
        }
        // запись размера картинки
        destf << QString("\n\t0x%1,0x%2,0x%3,0x%4, // size buffer %5\n\t")
            .arg((uint8_t)dest.size(),2,16,QLatin1Char('0'))
            .arg((uint8_t)(dest.size()>>8),2,16,QLatin1Char('0'))
            .arg((uint8_t)(dest.size()>>16),2,16,QLatin1Char('0'))
            .arg((uint8_t)(dest.size()>>24),2,16,QLatin1Char('0'))
            .arg(dest.size());
        if (!ui->rB_none->isChecked()){
            // запись таблицы ссылок на символы
            for (int i=0;i<16;i++){
                if (i%8==0) destf<<"\n\t";
                if (i<map.size()) destf << QString("0x%1,0x%2,")
                                           .arg((uint8_t)map[i],2,16,QLatin1Char('0'))
                                           .arg((uint8_t)(map[i]>>8),2,16,QLatin1Char('0'));
                else destf << QString("0x%1,0x%2,")
                              .arg((uint8_t)0,2,16,QLatin1Char('0'))
                              .arg((uint8_t)0,2,16,QLatin1Char('0'));
            }
        }
        // запись самой картинки
        step=dest.size()/50.0;
        for (int i=0;i<dest.size();i++){
            if (i%16==0) destf<<"\n\t";
            destf << QString("0x%1,").arg(dest[i],2,16,QLatin1Char('0'));
            ui->progressBar->setValue(100-dest.size()*step);
            }
    }
    else{
        // запись картинки
        step=buf.size()/50.0;
        for (int i=0;i<buf.size();i++){
            if (i%8==0) destf<<"\n\t";
            destf << QString("0x%1,").arg(buf[i],4,16,QLatin1Char('0'));
            ui->progressBar->setValue(100-buf.size()*step);
            }
    }
    destf<<"\n\t};\n";
    f_dest.close();
    f_source.close();
    f_str.close();
    ui->progressBar->setVisible(false);
    return (name_path+name_cut+".c");
}

/// заполнение словаря
void MainWindow::fillDictionary()
{
    QHash<uint16_t,uint32_t> table;
    // хеш заполняется уникальными значениями и количеством их повторов.
    for (int i=0;i<buf.size();i++) table.insert(buf[i],(table.value(buf[i],0))+1);
    // в цикле заполняется массив dictionary DICT самых встречаемых значений.
    for (int i=0;i<DICT;i++){
        uint32_t ind=0;
        QHash<uint16_t,uint32_t>::iterator k;
        QHash<uint16_t,uint32_t>::iterator n_iterator = table.begin();
        n_iterator=table.begin();
        while(n_iterator!=table.constEnd()){
            if (ind<n_iterator.value()){
                ind=n_iterator.value();
                k=n_iterator;
            }
            ++n_iterator;
        }
        dictionary[i]=k.key();
        table.erase(k);
        if (table.isEmpty()) break;
    }
}

/// процесс архивирования
int MainWindow::fillArc(int bias,int count)
{
    int b;
    int co=0;
    uint16_t val=buf[bias];
    for (int x=bias;x<count+bias-1;x++){
        uint16_t n=buf[x+1];
        b=DICT+1;
        for (int i=0;i<DICT;i++) if (dictionary[i]==val){b=i; break;}
        if (n!=val){ // записываем предыдущий байт
            if (b<DICT) dest.append(co*64+b+32);// есть в словаре
            else { // нет в словаре
                dest.append((uint8_t)val & 0xDF);
                dest.append((uint8_t)(val/256));
            }
            val=n;
            co=0;
        }
        else{ // совпадает с предыдущим
            if (b<DICT){ // есть в словаре
                co++;
                if (co>REPIT){
                    dest.append(0xE0+b);
                    co=0;
                }
            }
            else{
                // нет в словаре
                dest.append((uint8_t)val & 0xDF);
                dest.append((uint8_t)(val/256));
                co=0;
            }
        }
    }
return dest.count();
}


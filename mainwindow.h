#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>

#define DICT 32
#define REPIT 3

typedef struct  {
    uint16_t    bfType;
    uint32_t   bfSize;
    uint16_t    bfReserved1;
    uint16_t    bfReserved2;
    uint32_t   bfOffBits;
  } BITMAPFILEHEADER;

typedef struct  {
    uint32_t  biSize;
    int32_t   biWidth;
    int32_t   biHeight;
    uint16_t   biPlanes;
    uint16_t   biBitCount;
    uint32_t  biCompression;
    uint32_t  biSizeImage;
    int32_t   biXPelsPerMeter;
    int32_t   biYPelsPerMeter;
    uint32_t  biClrUsed;
    uint32_t  biClrImportant;
  } BITMAPINFOHEADER;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_all_clicked();

private:
    Ui::MainWindow *ui;
    QString saveFile(QString name);
    void fillDictionary(void);
    int fillArc(int,int);
    uint16_t dictionary[DICT];
    QVector<uint16_t> buf;
    QList<uint8_t> dest;

};

#endif // MAINWINDOW_H
